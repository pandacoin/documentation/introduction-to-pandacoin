- [Pandacoin (PND)](#pandacoin--pnd-)
  * [What is Pandacoin?](#what-is-pandacoin-)
    + [How does Pandacoin work?](#how-does-pandacoin-work-)
      - [Proof-of-Stake](#proof-of-stake)
      - [Proof-of-Work](#proof-of-work)
      - [The Benefits of Using Both](#the-benefits-of-using-both)
  * [The History of Pandacoin](#the-history-of-pandacoin)
    + [Launch](#launch)
      - [Why Pandacoin Exists](#why-pandacoin-exists)
      - [Proof-of-Stake Implementation](#proof-of-stake-implementation)
- [The Desktop Wallet](#the-desktop-wallet)
  * [Installation](#installation)
  * [Receiving Pandacoin](#receiving-pandacoin)
    + [Receiving PND from others](#receiving-pnd-from-others)
    + [Receiving PND from Telegram Wallet](#receiving-pnd-from-telegram-wallet)
  * [Sending Pandacoin](#sending-pandacoin)
    + [Sending PND to others](#sending-pnd-to-others)
    + [Sending PND to your Telegram Wallet](#sending-pnd-to-your-telegram-wallet)
  * [Encrypting your Pandacoin Wallet](#encrypting-your-pandacoin-wallet)
    + [Choosing a Wallet Password](#choosing-a-wallet-password)
    + [Storing your Wallet Password](#storing-your-wallet-password)
    + [Losing your Wallet Password](#losing-your-wallet-password)
  * [Backing up your Pandacoin Wallet](#backing-up-your-pandacoin-wallet)
    + [Creating wallet backups](#creating-wallet-backups)
    + [Backup notes](#backup-notes)
- [Android Wallet](#android-wallet)
  * [Installation](#installation-1)
  * [Receiving PND](#receiving-pnd)
    + [Receiving PND from others](#receiving-pnd-from-others-1)
    + [Receiving PND from Telegram Wallet](#receiving-pnd-from-telegram-wallet-1)
  * [Sending PND](#sending-pnd)
    + [Sending PND to your Telegram Wallet](#sending-pnd-to-your-telegram-wallet-1)
  * [Encrypting & Backing up your Wallet](#encrypting---backing-up-your-wallet)
    + [Choosing a Wallet Password](#choosing-a-wallet-password-1)
    + [Storing your Wallet Password](#storing-your-wallet-password-1)
    + [Losing your Wallet Password](#losing-your-wallet-password-1)
    + [Creating wallet backups](#creating-wallet-backups-1)
    + [Backup notes](#backup-notes-1)
- [iOS Wallet](#ios-wallet)
  * [Installation](#installation-2)
  * [Creating a Wallet](#creating-a-wallet)
    + [Choosing a Wallet Type](#choosing-a-wallet-type)
  * [Receiving PND](#receiving-pnd-1)
    + [Receiving PND from others](#receiving-pnd-from-others-2)
    + [Receiving PND from Telegram Wallet](#receiving-pnd-from-telegram-wallet-2)
  * [Sending PND](#sending-pnd-1)
    + [Sending PND to your Telegram Wallet](#sending-pnd-to-your-telegram-wallet-2)
  * [Encrypting & Backing up your Wallet](#encrypting---backing-up-your-wallet-1)
    + [Storing your Wallet Password](#storing-your-wallet-password-2)
    + [Losing your Wallet Password](#losing-your-wallet-password-2)
    + [Creating wallet backups](#creating-wallet-backups-2)
    + [Backup notes](#backup-notes-2)
- [Getting Pandacoins](#getting-pandacoins)
  * [Exchanges](#exchanges)
    + [Getting PND from Dex-trade](#getting-pnd-from-dex-trade)
    + [Withdrawing PND from Dex-trade](#withdrawing-pnd-from-dex-trade)
  * [Staking](#staking)
  * [Contributions](#contributions)
- [The Telegram Tip Bot](#the-telegram-tip-bot)
  * [Tipbot Commands](#tipbot-commands)
    + [Tipping](#tipping)
    + [Withdrawing](#withdrawing)
    + [Depositing](#depositing)
    + [Checking your Balance](#checking-your-balance)
    + [Donating](#donating)
- [Wildlife Donations](#wildlife-donations)
- [Folding@Home](#folding-home)
  * [Installation Guides](#installation-guides)
  * [Joining the Pandacoin Team](#joining-the-pandacoin-team)
      - [Example Configuration File](#example-configuration-file)


# Pandacoin (PND)

## What is Pandacoin?

Pandacoin is a decentralised, peer-to-peer digital cryptocurrency. It is an
environmentally friendly coin that aims to become universally adopted through its
fairness, honesty, and sustainability. Pandacoin's lead developer is Timon Saße (amDOGE).

### How does Pandacoin work?

The Pandacoin network uses a combination of the Proof-of-Stake and Proof-of-
Work (SHA256D) consensus mechanisms to validate new transactions and add them
to the Pandacoin blockchain.

#### Proof-of-Stake

In Proof-of-Stake, a node can announce new blocks to the network for validation. 
In order to ensure that this node does not create a fraudulent block, 
they must put a portion of their coins at stake when creating the block. Hence, why it is called Proof-of-Stake. 
If the network agrees that the block is valid, it will be added to the blockchain and 
the creator will get their stake back plus a reward for their work. 
On the other hand, if the network agrees that it is an invalid block, 
the network will disconnect from the offending node.
However, in more serious cases, the node may lose a portion of or even its entire stake.

#### Proof-of-Work

Alternatively, in Proof-of-Work, miners use their computer hardware to solve a
hash in order to verify a transaction's authenticity. The first miner to solve
the hash is given the block reward. As a result of the competitive nature, miners buy better
and better hardware in order to get more block rewards. This has caused concerns
about Proof-of-Work's energy consumption, in particular, Bitcoin. Bitcoin is the
most popular cryptocurrency and there are huge mining farms dedicated solely to
mining it. These mining farms use A LOT of electricity while mining for Bitcoin.


#### The Benefits of Using Both

Pandacoin uses both of these consensus mechanisms to ensure utmost network
security and to be a sustainable coin for all. Pandacoin has a mild bias for creating
PoS blocks. There is a forced 4:1 ratio of PoS to PoW blocks and the target is 6:1. 
To ensure that huge mining farms aren't built over mining PND, 
the reward for finding a PoW block has a theoretical limit of 999 PND. 
However, in practice the real limit is much lower at around 250 PND. 
The current highest PoW block reward can be seen on the [Pandacoin Blockchain Explorer](https://chainz.cryptoid.info/pnd/tx.dws?2.htm). 
Another interesting thing to note is the reward for finding a block is inversely linked to the difficulty of finding said block. 

## The History of Pandacoin

### Launch

Pandacoin launched in early 2014 with no Initial Coin Offerings (ICOs), no pre-mines 
or insta-mines to ensure a fair distribution of the Pandacoin supply.

As previously discussed, Pandacoin launched without an ICO, pre-mine or insta-mine to ensure a fair wealth distribution.

Let's take a look at what each of these terms mean and why they are detrimental to a fair supply distribution:

|Term|Definition|
| --- | --- |
|Initial Coin Offerings (ICOs):|The developer(s) allow people to buy a certain amount of the coin's supply before its launch in exchange for a more stable cryptocurrency such as BTC or ETH, or a fiat currency.|
|Pre-mines:|The developer(s) mine a portion (usually a large one) of the supply for themselves before the project gets officially launched to the wider community.|
|Insta-mines:|The developer(s) or dev team set the rewards for the first few blocks of the blockchain to have very high block rewards so that they can easily mine them.|

Naturally, these processes heavily skew the supply distribution of a coin towards the developers and early investors. Pandacoin launched with none of these.

#### Why Pandacoin Exists
In February of 2014, an individual named Wolong created a coin called Pandacoin
(PANDA). PANDA was a pump and dump scheme that many people unfortunately
fell for.

After seeing Wolong reap the benefits of his scams, a portion of the early Dogecoin
community decided that enough was enough. A group of developers led by Timon
Saße (amDOGE) quickly made a coin of their own also called Pandacoin (PND). The
purpose of this coin was to confuse new investors and thus stop Wolong's scams.
The plan worked well and PANDA's trade volumes plummeted. Eventually, it got
delisted from many exchanges which effectively killed the scam coin.

#### Proof-of-Stake Implementation

After a community vote, Pandacoin chose to implement the Proof-of-Stake consensus
mechanism to validate new transactions. This vote made Pandacoin one of the first
coins to implement Proof-of-Stake.


# The Desktop Wallet

To download the Pandacoin desktop wallet client, please go to
https://pandacoin.tech/wallet.

## Installation

Please choose the operating system relevant to you and click on it to download.

After you have downloaded the wallet, you should be able to click on the
downloaded file to run the Pandacoin wallet client.

If you can't find it, it should be located in your downloads folder.

![Wallet download selection](img/img-000-wallet-download-selection.png)

Upon downloading and installing your Pandacoin wallet, the wallet will
synchronise to the Pandacoin network. This should only take a few minutes.

After the sync is complete, you will be able to use your Pandacoin wallet.

Let's go over the basics of sending and receiving PND first.


## Receiving Pandacoin

To receive Pandacoin (PND) you will need a receiving Pandacoin address. In the
Pandacoin wallet client, select the 'Receive' section near the top of the screen. There
should be an option to 'Create a new receiving address'. Once you click that, your
address should appear like this:

![Receiving screen](img/img-001-receiving-dialog.png "Receiving screen")

This address shown here is your new Pandacoin address that you can use to receive
PND. In this example, my new Pandacoin address is:
pn1qdmu2cdk48w9984hc0atcck9tteu7srd7kk7fsn.

Don't worry, you won't need to type the address out every time you want to receive
PND. Clicking the 'Copy Address' button will copy the address to your clipboard.

Note: You can have as many receiving addresses as you'd like. It is good practice to
use different addresses for different payments.


### Receiving PND from others

If you would like to receive PND from other people, you can send them your
Pandacoin address. They will go to the 'send' section of the Pandacoin wallet client
and paste your address into the address box.

Optionally, you may also include an amount for a specific payment when creating a
receiving address. For example, you want your friend to send you 1000 PND. Go to
the 'receive' section and enter the amount you wish to request in the 'amount' box.
You may also give it a label such as 'Receiver for (friend's name)'.

![Receiving request](img/img-002-receiving-request.png "Receiving request")

In this example, I have created a receiving address called 'Receiver for amDOGE' to
receive a payment of 69 PND from amDOGE. To receive this payment, I need to
send him the URI using the 'Copy URI' button so that he can easily send the
requested amount to me. Alternatively, I could send him the QR code to scan.

### Receiving PND from Telegram Wallet

If you would like to withdraw the PND from your Telegram Wallet, you can send a
private message to the Pandacoin bot @PNDTip_bot on Telegram in the format
'/withdraw pn1qdmu2cdk48w9984hc0atcck9tteu7srd7kk7fsn 100', minus the
quotation marks.


## Sending Pandacoin

To send Pandacoin you will need a Pandacoin address to send to. In the Pandacoin
wallet client, select the 'Send' section near the top of the screen.

![Sending screen](img/img-003-sending-dialog.png "Sending screen")

### Sending PND to others

In this example, the address I would like to send PND to
'PXThkQ9LgP7t2xfFiHACPVH3eJjfPhzark' which is the address for the Pandacoin
Development Fund.

The label is set to 'Pandacoin Dev Fund' so that I can get the address from the
address book in future without the need to copy and paste the address.

### Sending PND to your Telegram Wallet

If you would like to send PND to your Telegram Wallet, you can send a private
message to the Pandacoin bot @PNDTip_bot on Telegram in the format '/deposit'.
The bot will give you a deposit address which you can use to send PND to.


## Encrypting your Pandacoin Wallet

You are in charge of your Pandacoin wallet. It is your responsibility to ensure that it
is safely backed up and secure from others.

In the Pandacoin wallet client, select the 'Options' option in the top left of your
screen. Then, click 'Encrypt Wallet'.

![Encrypting screen](img/img-004-encrypt-dialog.png "Encrypting screen")

### Choosing a Wallet Password

Please choose a very strong password when encrypting your wallet so that it is not
susceptible to bruteforce attacks. A password generator is highly recommended for
this.

### Storing your Wallet Password

Store your password in a safe location such as an encrypted thumb drive, password
manager or a sheet of paper that is stored in a safe location.

**DO NOT** store your wallet password in an unencrypted text file on your computer.

**DO NOT** tell anyone your password.

### Losing your Wallet Password

If you lose access to your password, you will lose access to your wallet. There is no
way to reset it.


## Backing up your Pandacoin Wallet

In the Pandacoin wallet client, select the 'File' option in the top left of your screen.
Then, click 'Backup Wallet'. Now, you should be able to choose a location to backup

![Save backup dialog](img/img-005-save-backup-dialog.png "Save backup dialog")

### Creating wallet backups

Create as many backups as possible in different locations. For example, one backup
on your local hard drive, one backup on a USB drive, another backup in cloud
storage and finally a backup to a CD.

The more backups you have, the more resilient you are to losing your wallet.

### Backup notes

Please keep in mind that conventional hard drives **DO NOT** last forever. They do fail
and sometimes without warning. That is why it is always good practice to have lots
of backups.

If you are looking for a great archival storage medium that will last a very long
time, you should have a look at M-Discs.


# Android Wallet

Aside from the traditional desktop wallet, Pandacoin also offers mobile wallets for
its users.To install the android wallet client, please visit:
[https://play.google.com/store/apps/details?id=de.amdoge.wallet](https://play.google.com/store/apps/details?id=de.amdoge.wallet).

## Installation

Clicking the link above, should direct you to the Pandacoin wallet on the play store.
From there, you can click 'Install' to install the wallet on your device.

![Install from PlayStore](img/img-006-android-install-from-playstore.png "Install from PlayStore")

Note: Bluewallet is currently being tested on Android and should release sometime
in the future.


## Receiving PND

To receive Pandacoin (PND) you will need a receiving Pandacoin address. In the
Pandacoin wallet client, select the OR code in the top right of your screen. Click on
the address below the QR code and there should be an option to copy it or to share
it with others via other platforms.

![Android receive dialog](img/img-007-android-receive-dialog.png "Android receive dialog")

The address shown here is your Pandacoin address that you can use to receive PND.

You can have as many receiving addresses as you'd like and your receiving address
will change after each transaction.

You can still use the same receiving address for transactions and all of your
receiving addresses are stored in your address book which can be accessed by
clicking the tag icon located to the right of the Pandacoin name.

![Android receiving addresses button](img/img-008-android-receiving-addresses-button.png "Android receiving addresses button")

### Receiving PND from others

If you would like to receive PND from other people, you can send them your
Pandacoin address. They will go to the 'send' section of their Pandacoin wallet
client and paste your address into the address box.


Optionally, you may also include an amount for a specific payment when creating a
receiving address. For example, you want your friend to send you 1000 PND. Click
the 'Request Coins' button located at the bottom of your screen and enter the
amount you wish to request in the 'amount' box.

![Android request dialog](img/img-009-android-request-dialog.png "Android request dialog")

In this example, I have created a request to receive a payment of 6969 PND. In order
for the other person to be able to send me the 6969 PND, I will need to send them
the URI using the 'Copy URI' button so that he can easily send the requested
amount to me. Alternatively, I could use the share button to share the URI with
them or they could scan the QR code from my phone.

### Receiving PND from Telegram Wallet

If you would like to withdraw the PND from your Telegram Wallet, you can send a
private message to the Pandacoin bot @PNDTip_bot on Telegram in the format
'/withdraw pn1qdmu2cdk48w9984hc0atcck9tteu7srd7kk7fsn 100', minus the
quotation marks.


## Sending PND

To send Pandacoin you will need a Pandacoin address to send to. In the Pandacoin
wallet client, select the 'Send Coins' button near the bottom of the screen.

![Android sending dialog](img/img-010-android-sending-dialog.png "Android sending dialog")

In this example, I am sending 6969 PND to the address
'PP2A8mWV2WoeJ1ThW7aJGTaSUwy2QeVS3T'. Please double check that the
address you are sending to is the correct address.

Note: There is no confirmation when clicking send so please make sure the amount
and address are correct.

### Sending PND to your Telegram Wallet

If you would like to send PND to your Telegram Wallet, you can send a private
message to the Pandacoin bot @PNDTip_bot on Telegram in the format '/deposit'.
The bot will give you a deposit address which you can use to send PND to.


## Encrypting & Backing up your Wallet

You are in charge of your Pandacoin wallet. It is your responsibility to ensure that it
is safely backed up and secure from others.

![Android password entry dialog](img/img-011-android-password-entry-dialog.png "Android password entry dialog")

In the Pandacoin wallet client, select the 'Options' button (3 dots) in the top right of
your screen. Then, click 'Safety' and 'Backup Wallet'.

### Choosing a Wallet Password

Please choose a very strong password when encrypting your wallet so that it is not
susceptible to bruteforce attacks. A password generator is highly recommended for
this.

### Storing your Wallet Password

Store your password in a safe location such as an encrypted thumb drive, password
manager or a sheet of paper that is stored in a safe location.

**DO NOT** store your wallet password in an unencrypted text file on your computer.

**DO NOT** tell anyone your password.

### Losing your Wallet Password

If you lose access to your password, you will lose access to your wallet. There is no
way to reset it.


### Creating wallet backups

Create as many backups as possible in different locations. For example, one backup
on your local hard drive, one backup on a USB drive, another backup in cloud
storage and finally a backup to a CD.

The more backups you have, the more resilient you are to losing your wallet.

### Backup notes

Please keep in mind that mobile phones **DO NOT** last forever. They do fail and
sometimes without warning. You may lose it or it may break. That's why it's always
good to have lots of backups.

If you are looking for a great archival storage medium that will last a very long
time, you should have a look at M-Discs.


# iOS Wallet

Currently, the iOS Bluewallet must be sideloaded via the Altstore.

## Installation

For instructions on how to install AltStore on your iOS device,
please visit the [AltStore FAQ](https://altstore.io/faq/).

## Creating a Wallet

To create your first wallet, click the 'Add New' button in the 'Add Wallet' section.

![iOS add wallet dialog](img/img-012-ios-add-wallet-dialog.png "iOS add wallet dialog")

### Choosing a Wallet Type

In the Bluewallet app, there are two options for creating a wallet: Pandacoin and
Vault.

IMPORTANT: Both of these wallets will give you 12 word mnemonic seed phrases
to store safely. You will need to write these down and store them somewhere safe
such as an encrypted USB or in a safe location that you know of. These seed phrases
can be used to gain access to your Pandacoins even if you no longer have access to
your wallet.

Pandacoin is a standard wallet that gives you one 12 word mnemonic seed phrase.
You will need this seed phrase to regain access to your funds if you lose your wallet.

Vault is a secure 2-of-3 multi-sig wallet that will give you three 12 word mnemonic
seed phrases. This means you will need two out of three of the 12 word mnemonic
seed phrases in order to regain access to your funds in the event that you lose your
wallet.

**NEVER SHARE YOUR SEED PHRASE WITH ANYONE.**

Note: The seed phrase must be in the correct order to work.


## Receiving PND

To receive Pandacoins from the Bluewallet app, you will a receiving address. Click
on your wallet from the main screen of the Bluewallet app and then click 'Receive'
on the bottom of your screen.

![iOS receive dialog](img/img-013-ios-receive-dialog.png "iOS receive dialog")

You can use the given address to receive PND.

### Receiving PND from others

If you would like to receive PND from other people, you can send them your
Pandacoin address. You can copy your Pandacoin address by clicking on it. There is
also a share button you can use to share your address. Alternatively, the person can
also scan your address with the QR code.

### Receiving PND from Telegram Wallet

If you would like to withdraw the PND from your Telegram Wallet, you can send a
private message to the Pandacoin bot @PNDTip_bot on Telegram in the format
'/withdraw pn1qdmu2cdk48w9984hc0atcck9tteu7srd7kk7fsn 100', minus the
quotation marks.


## Sending PND

To send Pandacoin you will need a Pandacoin address to send to. In the Pandacoin
wallet client, select the 'Send Coins' button near the bottom of the screen.

![iOS send dialog](img/img-014-ios-send-dialog.png "iOS send dialog")

In this example, I am sending 6969 PND to the address
'pn1q2wf3wdth2f2cg8zevz3gj37q48mrt5y3umnlap'.

Please double check that the address you are sending to is the correct address.

### Sending PND to your Telegram Wallet

If you would like to send PND to your Telegram Wallet, you can send a private
message to the Pandacoin bot @PNDTip_bot on Telegram in the format '/deposit'.
The bot will give you a deposit address which you can use to send PND to.


## Encrypting & Backing up your Wallet

You are in charge of your Pandacoin wallet. It is your responsibility to ensure that it
is safely backed up and secure from others.

In the Bluewallet client, select the 'Options' button (3 dots) in the top right of your
screen. Then, click 'Security' and check the switch for 'Encrypted and Password
Protected'. The app will prompt you to choose an encryption password.

![iOS password entry dialog](img/img-015-ios-password-entry-dialog.png "iOS password entry dialog")

### Storing your Wallet Password

Please choose a very strong password when encrypting your wallet so that it is not
susceptible to bruteforce attacks. A password generator is highly recommended for
this.

Store your password in a safe location such as an encrypted thumb drive, password
manager or a sheet of paper that is stored in a safe location.

**DO NOT** store your wallet password in an unencrypted text file on your computer.

**DO NOT** tell anyone your password.

### Losing your Wallet Password

If you lose access to your password, you will lose access to your wallet. There is no
way to reset it. You can only regain access to your funds with your seed phrases.

### Creating wallet backups

Create as many backups as possible in different locations. For example, one backup
on your local hard drive, one backup on a USB drive, another backup in cloud
storage and finally a backup to a CD.

The more backups you have, the more resilient you are to losing your wallet.

### Backup notes

Please keep in mind that mobile phones **DO NOT** last forever. They do fail and
sometimes without warning. You may lose it or it may break. That's why it's always
good to have lots of backups.

If you are looking for a great archival storage medium that will last a very long
time, you should have a look at M-Discs.


# Getting Pandacoins

There are many ways to get PND so let's go over the main ones:

## Exchanges

Buying Pandacoin from an exchange is the primary way to get PND. Here are the exchanges that Pandacoin is currently on:

* Dex-trade
* Altmarkets
* BitUBU

*Note: To buy PND from these exchanges you will need to already have some cryptocurrency such as Bitcoin (BTC), Ethereum (ETH), Tether (USDT), or Dogecoin (DOGE). If you do not currently have any crypto, you can get some via a fiat-to-crypto exchange.*

### Getting PND from Dex-trade

1. Go to [Dex-trade](https://www.dex-trade.com/).
2. Click the 'Sign up' located in the top right of your screen.
3. Create an account using your email and a **secure** password.
4. Go to [PND/USDT section](https://www.dex-trade.com/spot/trading/PNDUSDT) to trade USDT for PND.
5. Place an order in the 'Buy PND' section located in the bottom right of the screen.

### Withdrawing PND from Dex-trade
1. Go to the [wallets section](https://dex-trade.com/wallet).
2. Find Pandacoin (PND).
3. Click the 'Withdraw' button located on the right.
4. Get a receiving address from the wallet client that you wish to withdraw the PND to.
5. Paste the address from the wallet client into the address box on Dex-trade.
6. Select the amount that you wish to withdraw.
7. Once you have confirmed that the withdrawal address and amount are correct, select 'Request Withdraw'.

## Staking
You can earn Pandacoins passively through staking.  To stake your PND, you must have some PND in your **desktop** wallet. The wallet must be open and unlocked in the **desktop** client. Staking your PND should earn around 2.5% interest per annum. To earn the most staking rewards, your PC should always be on with the wallet client running. 

*Note: Compared to mining, the energy consumption from staking is negligible.*

## Contributions

You can also earn PND through contributions to the Pandacoin community such as PND artwork, memes, and spreading the word about Pandacoin. For example, Skye (@SkyeDose) has contributed some great Pandacoin art:

![Pandacoin Desktop Wallet by Skye](img/pndwooo.png)


# The Telegram Tip Bot

Pandacoin has a bot in the official Telegram group called PNDTip (@PNDTip_bot). You
can do a lot of cool things with the bot such as tipping other users and getting
helpful information.

## Tipbot Commands

|Command     |Description     |
| --- | --- |
|/hi|Say hello to the bot :)|
|/feedbamboo|Feed the bot some delicious bamboo!|
|/commands|Shows some example commands.|
|/help|Displays the full list of available commands.|
|/deposit|The bot will give a deposit PND address for your Telegram wallet.|
|/tip|Tip another user some PND from your Telegram wallet.|
|/withdraw|Withdraw funds from your Telegram wallet to a Pandacoin address.|
|/balance|Tells you the balance of your Telegram wallet.|
|/donate|Donate to the Pandacoin Development Fund.|
|/leaderboard|Displays the current donation leaderboard.|
|/exchanges|Gives you a list of exchanges that you can trade PND on.|
|/price|Tells you the current price of Pandacoin from CoinGecko.|
|/marketcap|Tells you the current marketcap of Pandacoin from CoinGecko.|
|/joke|Tells you a panda-related joke.|
|/bonk|Bonk another user.

### Tipping

To tip another user some PND from your telegram wallet, type:
```
/tip user amount
```

For example, to tip amDOGE 420.69 PND, type:
```
/tip @amDOGE 420.69
```


### Withdrawing

To withdraw PND from your Telegram wallet, type:
```
/withdraw address amount
```

For example, to withdraw 420.69 PND to the address PXThkQ9LgP7t2xfFiHACPVH3eJjfPhzark:
<br>
```
/withdraw PXThkQ9LgP7t2xfFiHACPVH3eJjfPhzark 420.69
```

*Note: Always double check the address before withdrawing.*

### Depositing

To deposit PND into your Telegram wallet, type:
```
/deposit
```

The bot will respond with the receiving address for your telegram wallet and a QR code.

In your Pandacoin wallet, go to the 'send' section and send the amount you wish to deposit into your Telegram wallet to the address given to you by the bot.


### Checking your Balance

To check your balance, type:
```
/balance
```

*Note: To check your balance, you must send the "/balance" command to the bot via a private message.*

### Donating
If you would to support the further development of Pandacoin with your PND you can do so by donating to the Pandacoin Development Fund. To donate to the fund with your Telegram wallet, type:
```
/donate 420.69
``` 

You can also donate to the fund via your regular wallets by sending PND to the address: PXThkQ9LgP7t2xfFiHACPVH3eJjfPhzark. 

All donations are greatly appreciated :)

# Wildlife Donations

The Pandacoin community wants to make a positive impact on the lives of pandas and animals in general. 
To reflect this, we have set up donation addresses for the World Wildlife Foundation, Chengdu Panda Base, and the Ocean Cleanup Project. 
You can donate to different these organisations using your PND. 

The funds raised here will be donated to their respective organisations 
at a future date to be decided by the Pandacoin Development Team. The donation wallets are being maintained by John Moore (Jommy99).

|Organisation|Pandacoin Address|
| --- | --- |
|World Wildlife Foundation|PG1HxBbH6fjJqx9taUrSqLx2Gmbg7DHc6x|
|Chengdu Panda Base|PN8QZ8UUpen5CpzY8m7nLPsu2qmxRTE6d3|
|The Ocean Cleanup Project|PSECCwBvFKCm9WtzDnWVfALgGSgbx2xHAf|

All donations are greatly appreciated :)

# Folding@Home

Folding@Home is a distributed computing project designed to help scientists develop new therapeutics for a multitude of diseases. This is achieved by simulating protein dynamics on the volunteers' hardware e.g. CPUs & GPUs. This includes the process of protein folding and the movements of proteins.

## Installation Guides
For an in-depth installation guide for your operating system please visit: [FAH Installation Guides](https://foldingathome.org/support/faq/installation-guides).

## Joining the Pandacoin Team
You can earn rewards in Pandacoin for folding as part of the Pandacoin FAH Team. To join the team, you must set up your FAH Client with the correct information. You can do this via the [FAH Web Client](https://client.foldingathome.org/), the FAHControl app, or your FAH 'config.xml'.

1. Set your team number to '234317'.
2. Set your username to a Pandacoin address you own to receive PND for your work.
3. Set a passkey for extra rewards. You can get a passkey via the [FAH Passkey Page](https://apps.foldingathome.org/getpasskey).

### Example Configuration File
```xml
<config>
 	
 	<fold-anon v="false"/>
 	<gpu v="true"/>
 	<!-- If true, attempt to autoconfigure GPUs -->
 	<power v="full"/>
 	<user v="PG1HxBbH6fjJqx9taUrSqLx2Gmbg7DHc6x"/>
 	<!-- Enter one of your pandacoin addresses to receive PND rewards.-->
 	<passkey v="69696969696969696969696969696969"/>
 	<!-- Enter your passkey here, do not share this.-->
	<team v="234317"/>
	<!-- This is the Pandacoin team number, do not change this.-->

	<slot id="0" type="CPU"/>
	
	<slot id="1" type="GPU">
		<pci-bus v="3"/>
  		<pci-slot v="0"/>
 	</slot>
	
	<slot id="2" type="GPU">
  		<pci-bus v="8"/>
  		<pci-slot v="0"/>
	</slot>

</config>
```
